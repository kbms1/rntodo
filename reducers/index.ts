import { combineReducers } from "redux";

import { todoLists } from "./todoLists";
import { todoListsItems } from "./todoListsItems";
import { listSorter } from "./listSorter";
import { visibilityFilter } from "./visibilityFilter";

export const rootReducer = combineReducers({
  listSorter,
  todoLists,
  todoListsItems,
  visibilityFilter
});
