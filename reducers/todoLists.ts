import { generate } from "shortid";
import { Action } from "redux";

import { CREATE_LIST, SET_TODO_LISTS, UPDATE_MODIFIED_DATE } from "../actions";

export interface CreateTodoListAction extends Action {
  type: "CREATE_LIST";
  name: string;
}

export interface SetTodoListAction extends Action {
  type: "SET_TODO_LISTS";
  lists: any[];
}

export interface UpdateListModifiedDate extends Action {
  type: "UPDATE_MODIFIED_DATE";
  listId: string;
}

export type TodoListAction =
  | CreateTodoListAction
  | SetTodoListAction
  | UpdateListModifiedDate;

export interface TodoList {
  id: string;
  name: string;
  lastModified: Date;
}

type ReducerState = TodoList[];

export function todoLists(
  state: ReducerState = [],
  action: TodoListAction
): any[] {
  switch (action.type) {
    case CREATE_LIST:
      if (action.name) {
        return [
          ...state,
          {
            id: generate(),
            name: action.name,
            lastModified: Date.now()
          }
        ];
      }
      return state;

    case SET_TODO_LISTS:
      return action.lists;

    case UPDATE_MODIFIED_DATE:
      const lastModified = Date.now();
      return state.map(
        list => (list.id === action.listId ? { ...list, lastModified } : list)
      );

    default:
      return state;
  }
}
