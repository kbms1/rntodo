import { Action } from "redux";

import { SET_LIST_SORTER } from "../actions";

export interface SortListAction extends Action {
  sorter: string;
}

export function listSorter(
  state: string = "List Name",
  action: SortListAction
) {
  switch (action.type) {
    case SET_LIST_SORTER:
      return action.sorter;
    default:
      return state;
  }
}
