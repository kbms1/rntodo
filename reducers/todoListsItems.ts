import { generate } from "shortid";

import {
  ADD_TODO_LIST_ITEM,
  SET_TODO_LIST_ITEMS,
  TOGGLE_TODO_LIST_ITEM
} from "../actions";

export function todoListsItems(state = {}, action) {
  switch(action.type) {
    case ADD_TODO_LIST_ITEM:
      return {
        ...state,
        [action.listId]: [
          ...state[action.listId],
          { id: generate(), description: action.todoText,  completed: false }
        ]
      };

    case TOGGLE_TODO_LIST_ITEM:
      const newItems = state[action.listId]
        .map(todo => todo.id === action.todoId ?
            { ...todo, completed: !todo.completed }
          : todo);
      return {
        ...state,
        [action.listId]: newItems
      };

    case SET_TODO_LIST_ITEMS:
      return {
        ...state,
        [action.listId]: action.items
      };

    default:
      return state;
  }
}
