import { Action } from "redux";
import { SET_VISIBILITY_FILTER } from "../actions";

export interface SetVisibilityAction extends Action {
  filter: string;
}

export function visibilityFilter(state = "All", action: SetVisibilityAction) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
}
