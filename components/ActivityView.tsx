import React from "react";
import { View, ActivityIndicator, ViewStyle } from "react-native";

interface Props {
  readonly style?: ViewStyle;
  readonly showIndicator: boolean;
  readonly indicatorSize?: "small" | "large";
  readonly indicatorStyle?: ViewStyle;
  readonly children: View[] | View;
}

export const ActivityView = (props: Props) => (
  <View style={props.style}>
    {props.showIndicator ? (
      <ActivityIndicator
        animating={true}
        size={props.indicatorSize}
        style={props.indicatorStyle}
      />
    ) : (
      props.children
    )}
  </View>
);
