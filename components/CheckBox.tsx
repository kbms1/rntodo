import React from "react";
import { TouchableOpacity, StyleSheet, ViewStyle } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

type Callback = () => any;

export interface CheckBoxProps {
  checked: boolean;
  onPress: Callback;
  style: ViewStyle;
}

export const CheckBox = ({ checked, onPress, style }: CheckBoxProps) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <Icon
      style={style}
      name={checked ? "check-square" : "square-o"}
      size={24}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    minWidth: 48,
    minHeight: 48
  }
});
