import React, { Component } from "react";
import {
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Text,
  Keyboard
} from "react-native";

type OnSubmitCallback = (text: string) => void;

interface Props {
  onSubmit: OnSubmitCallback;
  buttonLabel: string;
  placeholder: string;
  backgroundColor: string;
}

interface State {
  inputText: string;
}

export class InputWrapper extends Component<Props, State> {
  props: Props;

  state: State = {
    inputText: ""
  };

  updateInputText = (inputText: string) => this.setState({ inputText });

  onSubmit = () => {
    Keyboard.dismiss();
    this.props.onSubmit(this.state.inputText);
    this.setState({ inputText: "" });
  };

  render() {
    const {
      buttonLabel,
      placeholder,
      backgroundColor = "lightgray"
    } = this.props;
    return (
      <View style={[styles.wrapper, { backgroundColor: backgroundColor }]}>
        <TextInput
          value={this.state.inputText}
          autoCapitalize="sentences"
          autoCorrect={false}
          maxLength={40}
          underlineColorAndroid="transparent"
          onChangeText={this.updateInputText}
          onSubmitEditing={this.onSubmit}
          style={styles.inputBox}
          placeholder={placeholder}
        />
        <TouchableOpacity style={styles.addButton} onPress={this.onSubmit}>
          <Text>{buttonLabel}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4,
    paddingHorizontal: 16
  },
  inputBox: {
    flex: 1,
    backgroundColor: "#fff",
    height: 36,
    padding: 0,
    paddingLeft: 8,
    borderRadius: 4
  },
  addButton: {
    justifyContent: "center",
    alignItems: "center",
    minHeight: 48,
    minWidth: 48,
    marginLeft: 8
  }
});
