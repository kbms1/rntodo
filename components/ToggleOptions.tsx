import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextStyle,
  ViewStyle
} from "react-native";

type OnValueChangeCallback = (item: string) => void;

interface Props {
  style: ViewStyle;
  itemStyle: ViewStyle;
  value: string;
  activeItemStyle: ViewStyle;
  itemsSpacing: number;
  textStyle: TextStyle;
  activeTextStyle: TextStyle;
  items: string[];
  onValueChange: OnValueChangeCallback;
}

export const ToggleOptions = (props: Props) => {
  const renderItem = (item: string, index: number) => (
    <TouchableOpacity
      style={[
        props.itemStyle,
        item === props.value && props.activeItemStyle,
        index > 0 && { marginLeft: props.itemsSpacing }
      ]}
      key={item}
      onPress={() => props.onValueChange(item)}
    >
      <Text
        style={[props.textStyle, item === props.value && props.activeTextStyle]}
      >
        {item}
      </Text>
    </TouchableOpacity>
  );

  return <View style={props.style}>{props.items.map(renderItem)}</View>;
};
