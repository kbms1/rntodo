export const CREATE_LIST = "CREATE_LIST";
export const EDIT_LIST = "EDIT_LIST";
export const DELETE_LIST = "DELETE_LIST";
export const ADD_TODO_LIST_ITEM = "ADD_TODO_LIST_ITEM";
export const TOGGLE_TODO_LIST_ITEM = "TOGGLE_TODO_LIST_ITEM";
export const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";
export const UPDATE_MODIFIED_DATE = "UPDATE_MODIFIED_DATE";
export const SET_LIST_SORTER = "SET_LIST_SORTER";
export const SET_TODO_LISTS = "SET_TODO_LISTS";
export const SET_TODO_LIST_ITEMS = "SET_TODO_LIST_ITEMS";

export const VisibilityFilters = {
  All: "All",
  Completed: "Completed",
  Active: "Active"
};

export const ListsSorters = {
  LIST_NAME: "List Name",
  LAST_MODIFIED: "Last Modified"
};

export const createList = (name = "") => ({
  type: CREATE_LIST,
  name: name.trim()
});

export const setTodoLists = lists => ({
  type: SET_TODO_LISTS,
  lists
});

export const setTodoListItems = (listId, items) => ({
  type: SET_TODO_LIST_ITEMS,
  listId,
  items
});

export const addTodoListItem = (listId, todoText = "") => ({
  type: ADD_TODO_LIST_ITEM,
  listId,
  todoText: todoText.trim()
});

export const toggleTodoListItem = (listId, todoId) => ({
  type: TOGGLE_TODO_LIST_ITEM,
  listId,
  todoId
});

export const setVisibilityFilter = filter => ({
  type: SET_VISIBILITY_FILTER,
  filter
});

export const setListSorter = sorter => ({
  type: SET_LIST_SORTER,
  sorter
});

export const updateModifiedDate = listId => ({
  type: UPDATE_MODIFIED_DATE,
  listId
});
