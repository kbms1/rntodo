import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";

import {
  ToggleOptions,
  InputWrapper,
  CheckBox,
  ActivityView
} from "../components";
import { fetcher } from "../services";
import {
  setVisibilityFilter,
  addTodoListItem,
  toggleTodoListItem
} from "../actions";

interface ListDetailsItem {
  id: string;
  items: Todo;
}

interface Todo {
  id: string;
  completed: boolean;
  description: string;
}

interface ReduxAction {
  type: string;
}

type Dispatch = (action: ReduxAction) => void;

interface Props {
  navigation: any;
  visibilityFilter: string;
  allTodos: ListDetailsItem[];
  dispatch: Dispatch;
}

interface State {
  loading: boolean;
}

class ListDetails extends Component<Props, State> {
  state: State = {
    loading: true
  };

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.listName || "List Details"
  });

  componentWillMount() {
    this.setState({ loading: true });
    fetcher.getTodoListItems(this.props.navigation.state.params.listId);
  }

  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 3000);
  }

  onSubmit = (inputText: string) => {
    const listId = this.props.navigation.state.params.listId;
    this.props.dispatch(addTodoListItem(listId, inputText));
  };

  setVisibilityFilter = (filter: string) =>
    this.props.dispatch(setVisibilityFilter(filter));

  toggleCompleted = (todoId: string) => {
    const listId = this.props.navigation.state.params.listId;
    return () => this.props.dispatch(toggleTodoListItem(listId, todoId));
  };

  filteredItems = (items: Todo[] = []) => {
    const filter = this.props.visibilityFilter;
    if (filter === "All") {
      return items;
    } else {
      return items.filter(
        todo => (filter === "Completed" ? todo.completed : !todo.completed)
      );
    }
  };

  keyExtractor = (item: Todo) => item.id;

  renderItem = ({ item }) => {
    return (
      <View style={styles.listItem}>
        <CheckBox
          style={[item.completed && { color: "red" }]}
          checked={item.completed}
          onPress={this.toggleCompleted(item.id)}
        />
        <Text
          numberOfLines={1}
          style={[styles.todoText, item.completed && styles.completedTodoText]}
        >
          {item.description}
        </Text>
      </View>
    );
  };

  render() {
    const { listId } = this.props.navigation.state.params;
    const todos = this.filteredItems(this.props.allTodos[listId]);

    return (
      <View style={styles.container}>
        <InputWrapper
          buttonLabel="Add"
          backgroundColor="cyan"
          onSubmit={this.onSubmit}
          textInputProps={{ placeholder: "New item" }}
        />
        <ToggleOptions
          style={styles.visibilityFilters}
          items={["All", "Active", "Completed"]}
          value={this.props.visibilityFilter}
          activeItemStyle={styles.activeFilterButton}
          activeTextStyle={styles.activeFilterText}
          itemStyle={styles.filterButton}
          textStyle={styles.filterText}
          itemsSpacing={8}
          onValueChange={this.setVisibilityFilter}
        />
        <ActivityView
          style={{ paddingHorizontal: 16 }}
          showIndicator={this.state.loading}
          indicatorStyle={styles.spinner}
          indicatorSize="large"
        >
          {todos.length ? (
            <FlatList
              data={todos}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
              keyboardDismissMode="on-drag"
            />
          ) : (
            <Text style={{ padding: 8 }}>No data found!</Text>
          )}
        </ActivityView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  allTodos: state.todoListsItems,
  visibilityFilter: state.visibilityFilter
});

export const TodoListDetails = connect(mapStateToProps)(ListDetails);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  listItem: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "nowrap",
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  todoText: {
    flex: 1,
    marginLeft: 8
  },
  completedTodoText: {
    textDecorationLine: "line-through",
    color: "red"
  },
  filterButton: {
    flex: 1,
    borderColor: "blue",
    borderWidth: StyleSheet.hairlineWidth,
    height: 32,
    minWidth: 48,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    backgroundColor: "#fff"
  },
  activeFilterButton: {
    backgroundColor: "blue"
  },
  filterText: {
    color: "blue"
  },
  activeFilterText: {
    color: "#fff"
  },
  visibilityFilters: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 8,
    paddingHorizontal: 16
  },
  spinner: {
    alignSelf: "center",
    marginTop: 40
  }
});
