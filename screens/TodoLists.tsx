import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import moment from "moment";
import { sortBy } from "lodash";
import { connect } from "react-redux";

import { InputWrapper, ToggleOptions, ActivityView } from "../components";
import { fetcher } from "../services";
import { setListSorter, createList } from "../actions";

const formatDate = (date: Date) => moment(date).format("MMM DD, h:mm A");

interface ReduxAction {
  type: string;
}

type Dispatch = (action: ReduxAction) => void;

interface TodoList {
  id: string;
  name: string;
}

interface Props {
  dispatch: Dispatch;
  lists: TodoList[];
  sortType: string;
}

class TodoListsComp extends Component<Props> {
  componentWillMount() {
    fetcher.getTodoLists();
  }

  goToDetails = (item: TodoList) => () => {
    this.props.navigation.navigate("Details", {
      listId: item.id,
      listName: item.name
    });
  };

  keyExtractor = (item: TodoList) => item.id;

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={this.goToDetails(item)}
      >
        <Text style={styles.listName}>{item.name}</Text>
        <Text style={styles.lastModified}>{formatDate(item.lastModified)}</Text>
        <Icon name={"chevron-right"} color="lightgray" />
      </TouchableOpacity>
    );
  };

  setSortType = (sortType: string) =>
    this.props.dispatch(setListSorter(sortType));
  createList = (listName: string) => this.props.dispatch(createList(listName));

  render() {
    const { lists, sortType } = this.props;
    return (
      <View style={styles.container}>
        <InputWrapper
          buttonLabel="Add"
          onSubmit={this.createList}
          placeholder="New list name"
        />
        <ActivityView
          style={{ paddingHorizontal: 16 }}
          showIndicator={!lists}
          indicatorStyle={styles.spinner}
          indicatorSize="large"
        >
          <ToggleOptions
            style={styles.listHeader}
            items={["List Name", "Last Modified"]}
            value={sortType}
            itemStyle={styles.headerButton}
            activeItemStyle={styles.activeHeaderButton}
            textStyle={styles.headerText}
            activeTextStyle={styles.activeHeaderText}
            onValueChange={this.setSortType}
          />
          <FlatList
            data={sortBy(lists, [
              sortType === "List Name" ? "name" : "lastModified"
            ])}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />
        </ActivityView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  lists: state.todoLists,
  sortType: state.listSorter
});

export const TodoLists = connect(mapStateToProps)(TodoListsComp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  listHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 8,
    paddingBottom: 0,
    marginRight: 24
  },
  headerText: {
    color: "blue",
    paddingHorizontal: 5
  },
  activeHeaderText: {
    fontWeight: "100"
  },
  headerButton: {
    paddingBottom: 2
  },
  activeHeaderButton: {
    borderBottomWidth: 2,
    borderBottomColor: "blue"
  },
  listItem: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: StyleSheet.hairlineWidth,
    minHeight: 48
  },
  listName: {
    flex: 1
  },
  lastModified: {
    marginHorizontal: 12
  },
  spinner: {
    alignSelf: "center",
    marginTop: 40
  }
});
