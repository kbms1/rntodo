import React from "react";
import { StackNavigator } from "react-navigation";

import { TodoLists, TodoListDetails } from "../screens";


export const AppNavigator = StackNavigator({
  AllLists: {
    screen: TodoLists,
    navigationOptions: {
      headerTitle: "Todo Lists"
    }
  },
  Details: {
    screen: TodoListDetails,
    navigationOptions: {
      headerTitle: null
    }
  }
});

