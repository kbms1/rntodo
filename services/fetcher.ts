import SAMPLE_DATA from "../data/sampleData";
import { setTodoLists, setTodoListItems } from "../actions";
import { store } from ".";

export const fetcher = {
  getAllLists() {
    return new Promise(resolve => setTimeout(() => resolve(SAMPLE_DATA), 3000));
  },

  getTodoLists() {
    const { todoLists = [] } = store.getState();
    if (!todoLists.length) {
      setTimeout(() => {
        const todoLists = SAMPLE_DATA.map(list => ({
          id: list.id,
          name: list.name,
          lastModified: list.lastModified
        }));
        store.dispatch(setTodoLists(todoLists));
      }, 2000);
    }
  },

  getTodoListItems(listId) {
    const { todoListItems = {} } = store.getState();
    if (!todoListItems[listId]) {
      setTimeout(() => {
        const list = SAMPLE_DATA.find(list => list.id === listId) || {};
        store.dispatch(setTodoListItems(listId, list.items));
      }, 2000);
    }
  }
};
