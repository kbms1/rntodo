import Redux, { applyMiddleware, createStore } from "redux";
// import logger from "redux-logger";
// import { composeWithDevTools } from "redux-devtools-extension"; // for standalone debugger
import { composeWithDevTools } from "remote-redux-devtools"; // for use in vscode and other environments

import { rootReducer } from "../reducers";

const composeEnhancers = composeWithDevTools({
  // options here
});

export const store = createStore(
  rootReducer,
  composeEnhancers()
  // applyMiddleware(logger),
  // other store enhancers
);
