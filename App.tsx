import React, { Component } from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import { Provider } from "react-redux";

import { store } from "./services";
import { AppNavigator } from "./containers";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <AppNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  }
});
