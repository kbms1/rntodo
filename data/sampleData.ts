import { generate } from "shortid";

export default [
  {
    id: generate(),
    name: "Shopping List",
    lastModified: Date.now(),
    items: [
      {id: generate(), description: "Artichoke"},
      {id: generate(), description: "Asparagus"},
      {id: generate(), description: "Arugula"},
      {id: generate(), description: "Avocado"},
      {id: generate(), description: "Bamboo shoots"},
      {id: generate(), description: "Beets"},
      {id: generate(), description: "Bell peppers"},
      {id: generate(), description: "Bok choy"},
      {id: generate(), description: "Broccoli"},
      {id: generate(), description: "Brussels sprouts"},
      {id: generate(), description: "Cabbage"},
      {id: generate(), description: "Carrots"},
      {id: generate(), description: "Cassava"},
      {id: generate(), description: "Cauliflower"},
      {id: generate(), description: "Celery"},
      {id: generate(), description: "Chard"},
      {id: generate(), description: "Collard greens"},
      {id: generate(), description: "Corn"},
      {id: generate(), description: "Crisphead lettuce"},
      {id: generate(), description: "Cucumber"},
      {id: generate(), description: "Daikon"},
      {id: generate(), description: "Eggplant"},
      {id: generate(), description: "Endive"},
      {id: generate(), description: "Garlic"},
      {id: generate(), description: "Ginger"},
      {id: generate(), description: "Hot peppers"},
      {id: generate(), description: "Jicama"},
      {id: generate(), description: "Kale"},
      {id: generate(), description: "Kohlrabi"},
      {id: generate(), description: "Leaf lettuce"},
      {id: generate(), description: "Mushrooms"},
      {id: generate(), description: "Nopales"},
      {id: generate(), description: "Okra"},
      {id: generate(), description: "Onions"},
      {id: generate(), description: "Peas"},
      {id: generate(), description: "Potatoes"},
      {id: generate(), description: "Radishes"},
      {id: generate(), description: "Radicchio"},
      {id: generate(), description: "Romaine lettuce"},
      {id: generate(), description: "Shallots / Leeks"},
      {id: generate(), description: "Spinach"},
      {id: generate(), description: "Sprouts"},
      {id: generate(), description: "Squash"},
      {id: generate(), description: "Sweet potatoes"},
      {id: generate(), description: "Taro"},
      {id: generate(), description: "Tomatillo"},
      {id: generate(), description: "Tomatoes"},
      {id: generate(), description: "Turnips / Parsnips"},
      {id: generate(), description: "Water chestnuts"},
      {id: generate(), description: "Watercress"},
      {id: generate(), description: "Zucchini"}
    ]
  },
  {
    id: generate(),
    name: "Linux Distros to try",
    lastModified: Date.now(),
    items: [
      {id: generate(), description: "Arch Linux"},
      {id: generate(), description: "Debian"},
      {id: generate(), description: "Ubuntu"},
      {id: generate(), description: "OpenSUSE"},
      {id: generate(), description: "Fedora"}
    ]
  },
  {
    id: generate(),
    name: "Hugo Finalists To Read",
    lastModified: Date.now(),
    items: [
      {id: generate(), description: "The Obelisk Gate, by N. K. Jemisin"},
      {id: generate(), description: "All the Birds in the Sky, by Charlie Jane Anders"},
      {id: generate(), description: "Ninefox Gambit, by Yoon Ha Lee"},
      {id: generate(), description: "Death’s End, by Cixin Liu, translated by Ken Liu"},
      {id: generate(), description: "A Closed and Common Orbit, by Becky Chambers"},
      {id: generate(), description: "Too Like the Lightning, by Ada Palmer"},
    ]
  },
  {
    id: generate(),
    name: "NOT FOUND LIST",
    lastModified: Date.now(),
    items: []
  }
];
